$(function(){


	/*****************************************
        SCRIPTS SPOT DO PRODUTO
    *******************************************/
    $( "section.spotsProdutos ul.spots li" ).mouseenter(function(e){
       let imgFoco = $(this).children().children().children("img").attr("data-imgFoco");
       $(this).children().children().children("img").attr("src",imgFoco);
    }).mouseleave(function(e){
        let imgPadrao = $(this).children().children().children("img").attr("data-imgPadrao");
        $(this).children().children().children("img").attr("src",imgPadrao);
    });


    /*****************************************
		SCRIPTS PÁGINA INICIAL
	*******************************************/
	//CARROSSEL DE DESTAQUE
	$("#carrosselDestaque").owlCarousel({
		items : 1,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    //responsiveClass:true,			    
 //        responsive:{
 //            320:{
 //                items:1
 //            },
 //            600:{
 //                items:2
 //            },
           
 //            991:{
 //                items:2
 //            },
 //            1024:{
 //                items:3
 //            },
 //            1440:{
 //                items:4
 //            },
            			            
 //        }		    		   		    
	    
	});

	//CARROSSEL DE POSTS BLOG
	$("#carrosselPostsBlog").owlCarousel({
		items : 3,
        dots: true,
        loop: true,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,	       
	    autoplayTimeout:5000,
	    autoplayHoverPause:true,
	    smartSpeed: 450,

	    //CARROSSEL RESPONSIVO
	    responsiveClass:true,			    
        responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
           
            991:{
                items:2
            },
            1024:{
                items:2
            },
            1250:{
            	items:3
            },
            1440:{
                items:3
            },
            			            
        }		    		   		    
	    
	});
    //CARROSSEL DE POSTS BLOG
    $("#carrosselGaleriaProdutos").owlCarousel({
        items : 9,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,         
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        smartSpeed: 450,

        //CARROSSEL RESPONSIVO
        responsiveClass:true,               
        responsive:{
            320:{
                items:5
            },
            600:{
                items:6
            },
           
            991:{
                items:6
            },
            // 1024:{
            //     items:2
            // },
            // 1250:{
            //     items:3
            // },
            // 1440:{
            //     items:4
            // },
                                    
        }                               
        
    });

    //CARROSSEL DE PRODUTOS RELACIONADOS
    $("#carrosselProdutosRelacionados").owlCarousel({
        items : 4,
        dots: true,
        loop: true,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,         
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        smartSpeed: 450,

        //CARROSSEL RESPONSIVO
        responsiveClass:true,               
        responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
            991:{
                items:3
            },
            1024:{
                items:4
            },
            1200:{
                items:4
            },
            1440:{
                items:4
            },
       }
    });                        
        //BOTÕES DO CARROSSEL DE PRODUTOS RELACIONADOS
        var carrossel_produtos_relacionados = $("#carrosselProdutosRelacionados").data('owlCarousel');
        $('#esquerdaProdutosRelacionados').click(function(){ carrossel_produtos_relacionados.prev(); });
        $('#direitaProdutosRelacionados').click(function(){ carrossel_produtos_relacionados.next(); });

    $(".pgPrivacidade .secao-ajuda ul li .pergunta").click(function(e) {

        $(".pgPrivacidade .secao-ajuda ul li .pergunta").removeClass("ativo");

        $(this).addClass("ativo");

        $(".pgPrivacidade .secao-ajuda ul li .resposta").addClass("ativo");

        $(this).next().removeClass("ativo");

        $(".pgPrivacidade .secao-ajuda ul li").removeClass("ativo");

        $(this).parent().addClass("ativo");
    });


    /**********************************************************************
        SCRIPTS DO CARROSSEL DE PARCERIA
    **********************************************************************/
        
        if ($("#carrosselParceria").find('.item').length > 1) {
            $("#carrosselParceria").owlCarousel({
                items : 3,
                dots: false,
                loop: true,
                autoplay:true,
                lazyLoad: true,
                mouseDrag:true,
                touchDrag  : true,         
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                smartSpeed: 450,
            
                //CARROSSEL RESPONSIVO
            responsiveClass:true,               
            responsive:{
                320:{
                    items:1,
                    loop:true,
                },
                600:{
                    items:2,
                    loop:true,
                },
               
                991:{
                    items:2
                },
                1024:{
                    items:3 
                },
                1440:{
                    items:3
                },
                                        
            }   

            });


        }else{
            $("#carrosselParceria").owlCarousel({
                items : 3,
                dots: false,
                loop: true,
                autoplay:true,
                lazyLoad: true,
                mouseDrag:true,
                touchDrag  : true,         
                autoplayTimeout:3000,
                autoplayHoverPause:true,
                smartSpeed: 450,

                //CARROSSEL RESPONSIVO
                responsiveClass:true,               
                responsive:{
                    320:{
                        items:1,
                        loop:true,
                    },
                    600:{
                        items:2,
                        loop:true,
                    },
                   
                    991:{
                        items:2
                    },
                    1024:{
                        items:3 
                    },
                    1440:{
                        items:3
                    },
                                            
                }   

                
            });
        }
        //BOTÕES DO CARROSSEL DE PARCERIA
        var carrossel_parceria = $("#carrosselParceria").data('owlCarousel');
        $('#flechaEsquerda').click(function(){ carrossel_parceria.prev(); });
        $('#flechaDireita').click(function(){ carrossel_parceria.next(); });



	$(window).bind('scroll', function () {
     
     var alturaScroll = $(window).scrollTop();
     
       if (alturaScroll >= 200) {

          $(".topo").addClass("topoativo");

       } else {

            $(".topo").removeClass("topoativo");

       }
   });
    $(".pg-produto .produto .galeria figure img").click(function(e){
        let url_imagem = $(this).attr("src");
        console.log(url_imagem);
        $('.pg-produto .produto .imgImagemDestacada img').attr("src",url_imagem);
    });

    $(".pg-produto .produto .galeriaResponsiva .carrosselGaleriaProdutos .item img").click(function(e){
        let url_imagem = $(this).attr("src");
        console.log(url_imagem);
        $('.pg-produto .produto .imgImagemDestacada img').attr("src",url_imagem);
    });
    //$("#zoom_produto").zoom(".pg-produto .produto .imgImagemDestacada img");

});